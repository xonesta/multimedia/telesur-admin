# This is a multistage build. More info: https://malcoded.com/posts/angular-docker
ARG NODE_TAG=latest
ARG NGINX_TAG=stable-alpine
ARG USE_YARN
ARG BUILD_ENVIRONMENT=production

FROM node:${NODE_TAG:-latest} as build

# Copy NPM manifests and install dependencies
WORKDIR /app
COPY package*.json yarn.loc[k] ./
RUN if [ "x$USE_YARN" = "x" ]; then npm install --${BUILD_ENVIRONMENT:-production}; else yarn install --${BUILD_ENVIRONMENT:-production}; fi

# Copy source and build
COPY src src
COPY public public
RUN if [ "x$USE_YARN" = "x" ]; then npm run build; else yarn build; fi

# Copy the rest files
COPY . .

# Second stage (run)
FROM nginx:${NGINX_TAG}

ENV SERVER_NAME _
ENV SERVER_PORT 5000

# Copy build from previous stage
COPY --from=build /app/build /www

# Copy any nginx config
COPY site.conf.template .

CMD DOLLAR=$ envsubst < site.conf.template > /etc/nginx/conf.d/default.conf && echo "Starting nginx on port ${SERVER_PORT}" && exec nginx -g 'daemon off;'
